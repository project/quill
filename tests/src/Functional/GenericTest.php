<?php

declare(strict_types=1);

namespace Drupal\Tests\quill\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Generic module test for Quill module.
 *
 * @group quill
 */
class GenericTest extends BrowserTestBase {
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Checks installation about this module.
   */
  public function testModuleInstallation(): void {
    $module_installer = \Drupal::service('module_installer');
    // Check the installation of this module.
    $this->assertTrue($module_installer->install(['quill']));
    $info = \Drupal::service('extension.list.module')->getExtensionInfo('quill');
    if (!empty($info['required']) && !empty($info['hidden'])) {
      $this->markTestSkipped('Nothing to assert for hidden, required modules.');
    }

    if (empty($info['required'])) {
      // Check that the module can be uninstalled and then re-installed again.
      $this->assertTrue($module_installer->uninstall(['quill']), "Failed to uninstall Quill module");
      $this->assertTrue($module_installer->install(['quill']), "Failed to install Quill module");
    }
  }

}
