<?php

declare(strict_types=1);

namespace Drupal\Tests\quill\Functional;

use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;

/**
 * Generic module test for Quill module.
 *
 * @group quill
 */
class SettingTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'quill',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $adminUser;

  /**
   * The editor user.
   *
   * @var \Drupal\editor\Entity\Editor
   */
  protected Editor $editor;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $filtered_html_format = FilterFormat::create([
      'format' => 'quill_test',
      'name' => 'Quill Test Format',
      'filters' => [],
      'roles' => [RoleInterface::AUTHENTICATED_ID],
    ]);
    $filtered_html_format->save();
    $this->editor = Editor::create([
      'format' => 'quill_test',
      'editor' => 'quill',
    ]);
    $this->editor->save();

    // Create node type.
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    $this->adminUser = $this->drupalCreateUser([
      'administer filters',
      'create article content',
      'use text format quill_test',
    ]);
  }

  /**
   * Test basic settings.
   */
  public function testBasicSettings() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/content/formats/manage/quill_test');
    $assert_session->pageTextContains('Quill');

  }

}
